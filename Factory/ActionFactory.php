<?php
declare(strict_types=1);

namespace Factory;

use Enum\ActionEnum;

class ActionFactory
{
    /**
     * @param ActionEnum $action
     *
     * @return \Action\HomeAction
     * @throws \RuntimeException
     */
    public function buildAction(ActionEnum $action)
    {
        if ($action->getValue() === $action::HOME) {
            return new \Action\HomeAction();
        }

        throw new \RuntimeException('Cannot specify action for: '.$action->getValue());
    }
}
