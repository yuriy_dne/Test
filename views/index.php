<?php
/**
 * @var $resultMessage
 * @var $dinnerCosts
 */
?>
<form enctype="multipart/form-data" action="/" method="POST">
    <div>
        <label for="restaurantItems">Data file:</label>
        <input name="restaurantItems" id="restaurantItems" type="file" />
    </div>
    <div>
        <label for="dinnerItems">Dinner items:</label>
        <input name="dinnerItems" type="text" id="dinnerItems" />
    </div>
    <input type="submit" value="Find price" />
</form>

<?php if(!empty($resultMessage)): ?>
    <div class="result"><?=$resultMessage;?></div>
<?php endif;?>

<?php if(!empty($dinnerCosts)): ?>
    <div class="dinner-costs">
        <?php foreach ($dinnerCosts as $restaurantId => $dinnerCost): ?>
            <div> Restaurant: <?=$restaurantId?> </div>
            <div> Total cost: <?=$dinnerCost;?> </div>
            <hr />
        <?php endforeach;?>
    </div>
<?php endif;?>
