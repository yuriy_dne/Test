<?php
declare(strict_types=1);

namespace Model;

class MenuItem
{
    /**
     * @var int
     */
    private $restaurantId;

    /**
     * @var float
     */
    private $price;

    /**
     * @var string
     */
    private $label;

    /**
     * MenuItem constructor.
     *
     * @param int $restaurantId
     * @param float $price
     * @param string $label
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(
        $restaurantId,
        $price,
        $label
    ) {
        if (!is_int($restaurantId)) {
            throw new \InvalidArgumentException('restaurantId must be int');
        }
        if (!is_float($price) || $price <= 0) {
            throw new \InvalidArgumentException('price must be float and greater than 0');
        }
        if (!is_string($label) || empty($label)) {
            throw new \InvalidArgumentException('label must be not empty string');
        }

        $this->restaurantId = $restaurantId;
        $this->price = $price;
        $this->label = $label;
    }

    /**
     * @return int
     */
    public function getRestaurantId()
    {
        return $this->restaurantId;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
}
