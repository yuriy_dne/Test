<?php

namespace Action;

abstract class AbstractAction
{
    abstract public function run();

    const VIEW_PATS = '../views/';

    /**
     * @param string $viewName
     * @param null|array $params
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function render($viewName, array $params = null)
    {
        if (!is_string($viewName)) {
            throw new \InvalidArgumentException('param viewName must be string');
        }

        $viewPath = __DIR__.'/'.self::VIEW_PATS.$viewName.'.php';
        if (!is_file($viewPath)) {
            throw new \RuntimeException(
                sprintf('view file %s not found', $viewPath)
            );
        }

        extract($params, EXTR_SKIP);

        require $viewPath;
    }
}
