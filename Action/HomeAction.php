<?php

namespace Action;

use Service\DinnerPriceFounder;
use Service\RestaurantMenuCsvReader;

class HomeAction extends AbstractAction
{
    public function run()
    {
        $resultMessage = '';
        $dinnerCosts = [];
        if (!empty($_POST)) {
            try {
                $dinnerItemsRaw = explode(',',$_POST['dinnerItems']);
                $dinnerItems = [];
                foreach ($dinnerItemsRaw as $item) {
                    $item = trim($item);
                    if (!empty($item)) {
                        $dinnerItems[] = $item;
                    }
                }

                if (empty($dinnerItems)) {
                    $resultMessage = 'No dinner items were entered';
                } else {
                    $tempFilePath = __DIR__.'/../tmp/tmp.csv';
                    $fileContent = file_get_contents($_FILES['restaurantItems']['tmp_name']);
                    file_put_contents($tempFilePath, $fileContent);
                    $csvReader = new RestaurantMenuCsvReader($tempFilePath);
                    $priceFounder = new DinnerPriceFounder();
                    $dinnerCosts = $priceFounder->find(
                        $dinnerItems,
                        ...$csvReader->getMenuItems()
                    );

                    unlink($tempFilePath);

                    if (empty($dinnerCosts)) {
                        $resultMessage = 'No restaurant found for dinner';
                    }
                }
            } catch (\Throwable $e) {
                throw $e;
                $resultMessage = 'Find dinner price error: '.$e->getMessage();
            }
        }

        $this->render('index', [
            'resultMessage' => $resultMessage,
            'dinnerCosts'   => $dinnerCosts,
        ]);
    }
}
