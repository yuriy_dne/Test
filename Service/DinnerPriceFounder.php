<?php
declare(strict_types=1);

namespace Service;

use Model\MenuItem;

class DinnerPriceFounder
{
    const FIELD_DISHES_COUNT = 'dishesCount';
    const FIELD_DINNER_SUM = 'dinnerSum';

    /**
     * @param array $itemLabels
     * @param MenuItem[] ...$menuItems
     *
     * @return array in format restaurantId => dinnerSum
     */
    public function find(array $itemLabels, MenuItem ...$menuItems)
    {
        $dinnerSumByRestaurant = [];
        $itemLabelKeys = array_flip($itemLabels);

        foreach ($menuItems as $menuItem) {
            if (array_key_exists($menuItem->getLabel(), $itemLabelKeys)) {
                $restaurantId = $menuItem->getRestaurantId();
                if (!isset($dinnerSumByRestaurant[$restaurantId])) {
                    $dinnerSumByRestaurant[$restaurantId] = [
                        self::FIELD_DISHES_COUNT => 0,
                        self::FIELD_DINNER_SUM   => 0,
                    ];
                }
                $dinnerSumByRestaurant[$restaurantId][self::FIELD_DINNER_SUM]+= $menuItem->getPrice();
                $dinnerSumByRestaurant[$restaurantId][self::FIELD_DISHES_COUNT]++;
            }
        }

        $countItems = count($itemLabels);
        $restaurantsForDinner = [];
        foreach ($dinnerSumByRestaurant as $restaurantId => $data) {
            if ($data[self::FIELD_DISHES_COUNT] === $countItems) {
                $restaurantsForDinner[$restaurantId] = $data[self::FIELD_DINNER_SUM];
            }
        }

        asort($restaurantsForDinner);
        $minimalPrice = reset($restaurantsForDinner);
        $result = [];
        foreach ($restaurantsForDinner as $restaurantId => $price) {
            if ($price > $minimalPrice) {
                break;
            }
            $result[$restaurantId] = $price;
        }

        return $result;
    }
}
