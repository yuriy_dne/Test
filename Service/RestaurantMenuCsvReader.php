<?php
declare(strict_types=1);

namespace Service;

use Model\MenuItem;

class RestaurantMenuCsvReader
{
    /**
     * @var string
     */
    private $csvFileName;

    /**
     * CsvReader constructor.
     *
     * @param string $csvFileName
     *
     * @throws \RuntimeException
     */
    public function __construct($csvFileName)
    {
        if (!is_file($csvFileName)) {
            throw new \RuntimeException("file $csvFileName not found");
        }

        $this->csvFileName = $csvFileName;
    }

    /**
     * @return MenuItem[]
     * @throws \RuntimeException
     */
    public function getMenuItems()
    {
        $result = [];
        $handle = fopen($this->csvFileName, 'r');
        while ($csvRow = fgetcsv($handle, null, ',')) {
            $restaurantId = (int) $csvRow[0];
            $price = (float) $csvRow[1];
            for ($i=2; $i<count($csvRow); $i++) {
                try {
                    $model = new MenuItem(
                        (int)$restaurantId,
                        (float)$price,
                        trim($csvRow[$i])
                    );
                    $result[] = $model;
                } catch (\Throwable $e) {
                    throw new \RuntimeException('Cannot build menu item model for row: '.implode(',',$csvRow), $e);
                }
            }
        }
        return $result;
    }
}
