<?php
use Enum\ActionEnum;
use Factory\ActionFactory;

require_once(__DIR__.'/autoloader.php');

$actionName = isset($_REQUEST['action']) ? urldecode($_REQUEST['action']) : ActionEnum::HOME;
$actionFactory = new ActionFactory();

$actionFactory->buildAction(new ActionEnum($actionName))
    ->run();
?>
